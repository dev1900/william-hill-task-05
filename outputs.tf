output "vpc_id" {
  value = module.vpc.vpc_id
}

output "kms_key_arn" {
  value = aws_kms_key.this_key.arn
}