variable "access_key" {
  type = string
  description = "AWS Access Key credential"
}

variable "secret_key" {
  type = string
  description = "AWS Secret Key credential"
}

variable "region" {
  type = string
  description = "AWS region, where resources will be created"
  default = "eu-west-1"
}

variable "common" {
  description = "Common name for resources"
  type = string
}

variable "env" {
  description = "Environment"
  type = string
}

variable "vpc_azs" {
  description = "VPC availability zones"
  type = list(string)
}

variable "vpc_cidr" {
  description = "VPC cidr blocks"
  type = string
}

variable "vpc_private_subnets" {
  description = "VPC private subnets"
  type = list(string)
}

variable "vpc_public_subnets" {
  description = "VPC public subnets"
  type = list(string)
}
