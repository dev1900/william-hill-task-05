module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.6.0"

  name                 = "${var.common}-vpc-${var.env}-${var.region}"
  azs                  = var.vpc_azs
  cidr                 = var.vpc_cidr
  private_subnets      = var.vpc_private_subnets
  public_subnets       = var.vpc_public_subnets
  enable_nat_gateway   = true
  enable_vpn_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "${var.common}-security-group-${var.env}-${var.region}"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = module.vpc.vpc_id

}

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name                   = "${var.common}-ec2-${var.env}-${var.region}"
  ami                    = "ami-01efa4023f0f3a042"
  instance_type          = "t2.micro"
  monitoring             = true
  vpc_security_group_ids = [module.security_group.security_group_id]
  subnet_id              = element(module.vpc.private_subnets, 0)
  iam_instance_profile   = aws_iam_instance_profile.role_profile.name
}

resource "aws_kms_key" "this_key" {
  description             = "${var.common}-kms-key-${var.env}-${var.region}"
  deletion_window_in_days = 10
}

module "dynamodb_table" {
  source = "terraform-aws-modules/dynamodb-table/aws"

  name     = "${var.common}-dynamodb-${var.env}-${var.region}"
  hash_key = "id"
  attributes = [
    {
      name = "id"
      type = "N"
    }
  ]
}

resource "aws_iam_policy" "kms_access" {
  name        = "KMS_access"
  path        = "/"
  description = "Policy for KMS"

  policy = data.aws_iam_policy_document.kms_document.json
}

data "aws_iam_policy_document" "kms_document" {
  statement {
    sid = "Allow"

    actions = [
      "kms:Decrypt"
    ]

    resources = ["${aws_kms_key.this_key.arn}"]
  }
}

resource "aws_iam_policy" "dynamodb_access" {
  name        = "${var.common}-dynamodb-access-${var.env}-${var.region}"
  path        = "/"
  description = "Policy for DynamoDb"

  policy = data.aws_iam_policy_document.dynamodb_document.json
}

data "aws_iam_policy_document" "dynamodb_document" {
  statement {
    sid = "Allow"

    actions = [
      "dynamodb:GetRecords",
      "dynamodb:GetItem",
      "dynamodb:PutItem"
    ]

    resources = ["${module.dynamodb_table.dynamodb_table_arn}"]
  }
}

resource "aws_iam_role" "ec2_role" {
  name = "${var.common}-ec2-role-${var.env}-${var.region}"

  assume_role_policy =data.aws_iam_policy_document.role_policy.json

}

data "aws_iam_policy_document" "role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

  }
}

resource "aws_iam_role_policy_attachment" "dynamo_attach" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = aws_iam_policy.dynamodb_access.arn
}

resource "aws_iam_role_policy_attachment" "kms_attach" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = aws_iam_policy.kms_access.arn
}

resource "aws_iam_instance_profile" "role_profile" {
  role = aws_iam_role.ec2_role.name
}
